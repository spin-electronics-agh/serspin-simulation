function [ voltage, state ] = CIMS_current_model( current, state, model )
%CIMS_current current based cims voltage calculation
%   voltage - input voltage
%   state - current state (0 or 1)
%   model - element model structure
%   current - calculated current
%   stateOut - state after calculation

    criticalPlus = model.cP;
    criticalMinus = model.cM;

    if current > criticalPlus
        state = 1;
    elseif current < criticalMinus
        state = 0;
    end
    
    if state == 1
        if current > 0
            a = model.a1p;
            b = model.b1p;
        else
            a = model.a1n;
            b = model.b1n;
        end
    else
        if current > 0
            a = model.a0p;
            b = model.b0p;
        else
            a = model.a0n;
            b = model.b0n;
        end
    end
    
    voltage = (current*b)/(1 - current*a);


end

