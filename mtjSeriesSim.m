function [ currents, swHistory ] = mtjSeriesSim( voltages, mtjBase, num_MTJ )



currents = zeros(1,numel(voltages));

current_step = 1e-7;
current_max = 1e-2;


state = zeros(1, num_MTJ);
stateNew = zeros(1, num_MTJ);
current_act = 0;
%voltage_act = zeros(1, num_MTJ);
v_last = 0;
i = 1;

if voltages(1) > 0
    dir = 1;
else
    dir = -1;
end

swHistoryCounter = 0;

swHistory = [];

for v = voltages
    %current_act = 0;
    voltage_act = zeros(1, num_MTJ);
    while 1
        % detect change of direction of voltage vector - if so reset
        % previous state to prevent higher solution from being detected as
        % actual solution
        if v > v_last
            if dir == -1
               v_last = 0;
               current_act = 0;
            end
            dir = 1;
        elseif v < v_last
            if dir == 1
               v_last = 0;
               current_act = 0;
            end
            dir = -1;
        else
            %break
        end
        
        
        do_contiune = 0;  % for each MTJ calculate its voltage for given current
        for k = 1:num_MTJ
            [voltage_act(k), stateNew(k)] = CIMS_current_model(current_act, state(k), mtjBase(k));
            if ~isequal(state, stateNew)  % if switching detected calculation must be restarted for given voltage
                current_act = 0;
                v_last = 0;
                state = stateNew;
                do_contiune = 1;
                % save switching point (voltage & elements state) for
                % further purposes
                if swHistoryCounter > 0
                    if swHistory(swHistoryCounter).v ~= v
                        swHistoryCounter = swHistoryCounter + 1;
                        swHistory(swHistoryCounter).state = stateNew;
                        swHistory(swHistoryCounter).v = v;
                    else
                        swHistory(swHistoryCounter).state = stateNew;
                    end
                else
                    swHistoryCounter = swHistoryCounter + 1;
                    swHistory(swHistoryCounter).state = stateNew;
                    swHistory(swHistoryCounter).v = v;
                end
                break;
            end
        end
        
        if do_contiune == 1
           continue 
        end
        
        state = stateNew;
        
        % if calculated voltage over given voltage - we have found the
        % solution
        if abs(sum(voltage_act)) >= abs(v)
            currents(i) = current_act;
            break
        end
        % if current to high - error - no solution
        if abs(current_act) > current_max
            currents(i) = 0;
            break 
        end
        
        % according to given voltages vector increase or decrease current
        if v > v_last
            current_act = current_act + current_step;
        elseif v < v_last
            current_act = current_act - current_step;
        else
            %break
        end
    end
    
    % for detecting direction of vector remember actual voltage
    v_last = v;
    i = i + 1;
end




end

