function [ a, b ] = points2line( xa, ya, xb, yb )
%points2line finds line passing two points
%   -
    
    a = (ya-yb)/(xa-xb);
    b = ya - a*xa;

end

