function [ swHistory ] = mtjSeriesReadout(mtjBase, num_MTJ, swHistory, readVoltage )

current_step = 1e-7;
current_max = 1e-2;

voltage_act = zeros(1, num_MTJ);

historyIndex = 1;

% for each history element (ie switching)
for History = swHistory
    current_act = 0;
    state = History.state;
    
    while 1
        for k = 1:num_MTJ
            voltage_act(k) = CIMS_current_model(current_act, state(k), mtjBase(k));
        end
        
        %found solution
        if abs(sum(voltage_act)) >= abs(readVoltage)
            swHistory(historyIndex).read = sum(voltage_act)/current_act;
            break;
        end
        
        % reached current limit
        if abs(current_act) > current_max
            swHistory(historyIndex).read = 0;
            break;
        end
        
        if readVoltage > 0
            current_act = current_act + current_step;
        else
            current_act = current_act - current_step;
        end
        
    end
    
    historyIndex = historyIndex + 1;
end

end
