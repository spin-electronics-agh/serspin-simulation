# Serspin Simulation

A simulation software for serially connected magnetic tunnel junctions

See: [https://arxiv.org/abs/2102.03415](https://arxiv.org/abs/2102.03415)