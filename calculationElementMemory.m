clc, clear, close all;

% generate voltage vector
maxVolt = 4;
voltages = [0.02:0.001:maxVolt maxVolt:-0.05:0.02 -0.02:-0.001:-maxVolt -maxVolt:0.05:-0.02];

% define voltage used for simulated readout
readVoltage = 0.1;

num_MTJ = 7;

for i=1:num_MTJ
    % data for each of MTJ, according to model
    mtjBase(i).cP = 0.000144701903243697;
    mtjBase(i).cM = -8.73544996719471e-05;

    [a, b] = points2line(0.04996, 2452, 0.4, 1879);
    mtjBase(i).a1p = a;
    mtjBase(i).b1p = b;

    [a, b] = points2line(-0.05137, 2428, -0.1928, 2207);
    mtjBase(i).a1n = a;
    mtjBase(i).b1n = b;

    [a, b] = points2line(0.04993, 1081, 0.1557, 1076);
    mtjBase(i).a0p = a;
    mtjBase(i).b0p = b;

    [a, b] = points2line(-0.04991, 1082, -0.4, 1049);
    mtjBase(i).a0n = a;
    mtjBase(i).b0n = b;
end

% do the simulation and calculate I(V) and switching voltages
[currents, swHistory] = mtjSeriesSim(voltages, mtjBase, num_MTJ);

% simulate the readout
swHistory = mtjSeriesReadout(mtjBase, num_MTJ, swHistory, readVoltage);

% plot R(V)
hold on;
plot(voltages, voltages./currents, '-r');
% mark switching points vesrus obtained state for readout
plot([swHistory.v], [swHistory.read], '*b');
